# compile_fia_data

Execute the scripts inside `R` in order.
You will need internet connection and a lot of patience to recreate all datasets.
The compiled final datasets are:
  - `data/clean/fia/trees.rds`
  - `data/clean/fia/plot_lookup.rds`
  - `data/clean/fia/species_lookup.rds`
  
I'm using git-lfs to version control *everything* inside 'data/' (git lfs track 'data/')

Email me if you have any questions
Dudu (jemeireles@gmail.com)
